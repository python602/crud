# Curso Práctico de Python: Creación de un CRUD

Curso [Platzi](https://platzi.com/cursos/python-practico/)

## Instalation

```bash
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
```

## Execute scripts

En la raiz del directorio ejecutar `python` + "el nombre del archivo"

```bash
python main.py
```

## Platzi ventas

Moverse al directorio "platzi-ventas"

```bash
pip install --editable .
pv clients --help
```
