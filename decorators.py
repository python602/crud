PASSWORD = '159357456258'


def password_required(func):
    def wrapper():
        password = input('Cual e stu contrasena? ')

        if password == PASSWORD:
            return func()
        else:
            print('La contrasena no es correcta.')

    return wrapper


@password_required
def needs_password():
    print('La contrasena es correcta')


def upper(func):
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)

        return result.upper()

    return wrapper


@upper
def say_hello(name):
    return 'Hola, {}'.format(name)


if __name__ == '__main__':
    print(say_hello('Lisa'))
